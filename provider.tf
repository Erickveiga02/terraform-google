

provider "google" {
  credentials = "${file("velvety-transit-259623-721ccf865a39.json")}"
  project     = "velvety-transit-259623"
  region      = "us-west1-a"
}